# **CAR-SHARING-DATAART**

**This project is created for improving skills and demonstration
of developer proficiency.**

**The main purposes of creation are:**

- give car-sharing functionality for users;
- to provide easy to understand application for deals which include temporary usage of another user`s car for money compensation;
- fast and high-quality solution of disputable situations between users;
- ensuring the confidentiality of communication between users.

**To start the project you need:**

- Download and install Docker Desktop;
- Download PostgreSQL image to run docker-compose container;
- Check dependencies in build.gradle file;
- Make sure that Java 11 is being used;
- Type command "gradlew clean build" in terminal to build the application;
- Run docker-compose containers by typing the command "docker-compose up -d" in the terminal;
- After starting the project, check that the flyway scripts have worked correctly;
- Check through pgAdmin that a database has been created in the container, and you can connect to it.

**Technologies:**

- Spring Boot
- Spring Data Jpa
- Spring Security(JWT)
- Docker-compose
- Flyway
- PostgreSQL
- Lombok

###### Mentor: Alexander Yushko

###### Developer: Denys Suk