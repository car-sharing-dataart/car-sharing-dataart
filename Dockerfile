FROM amazoncorretto:11.0.12
ADD build/libs/car-share-dataart-01.02.00.jar car-share-dataart-01.02.00.jar
ENTRYPOINT ["java", "-jar", "car-share-dataart-01.02.00.jar"]