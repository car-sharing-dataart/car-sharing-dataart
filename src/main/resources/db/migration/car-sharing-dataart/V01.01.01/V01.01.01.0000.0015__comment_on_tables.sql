COMMENT ON TABLE users IS 'Список пользователей';
COMMENT ON COLUMN users.id IS 'Идентификатор пользователя';
COMMENT ON COLUMN users.first_name IS 'Имя пользователя';
COMMENT ON COLUMN users.last_name IS 'Фамилия пользователя';
COMMENT ON COLUMN users.middle_name IS 'Отчество пользователя';
COMMENT ON COLUMN users.login IS 'Логин пользователя';
COMMENT ON COLUMN users.password IS 'Пароль пользователя';
COMMENT ON COLUMN users.verified IS 'Верифицирован ли пользователь для аренды/сдачи авто';
COMMENT ON COLUMN users.birth_date IS 'Дата рождения пользователя';
COMMENT ON COLUMN users.email IS 'Почта пользователя';
COMMENT ON COLUMN users.mobile_phone IS 'Мобильній телефон пользователя';
COMMENT ON COLUMN users.sex IS 'Пол пользователя';
COMMENT ON COLUMN users.create_ts IS 'Дата регистрации';
COMMENT ON COLUMN users.avatar_path IS 'URL фотографии в профиле';
COMMENT ON COLUMN users.driver_license_id IS 'Водительские права пользователя';




COMMENT ON TABLE cars IS 'Список машин пользователей';
COMMENT ON COLUMN cars.id IS 'Идентификатор машины';
COMMENT ON COLUMN cars.brand IS 'Марка машины';
COMMENT ON COLUMN cars.model IS 'Модель машины';
COMMENT ON COLUMN cars.number IS 'Номер машины';
COMMENT ON COLUMN cars.vin IS 'VIN номер машины';
COMMENT ON COLUMN cars.year IS 'Год производства';
COMMENT ON COLUMN cars.colour IS 'Цвет машины';
COMMENT ON COLUMN cars.type IS 'Тип машины(Купе, внедорожник)';
COMMENT ON COLUMN cars.city IS 'Город, в котором находится машина';



COMMENT ON TABLE images IS 'Список изображений машин пользователей';
COMMENT ON COLUMN images.id IS 'Идентификатор изображения';
COMMENT ON COLUMN images.url IS 'URL изображения';
COMMENT ON COLUMN images.car_id IS 'Идентификатор машины';



COMMENT ON TABLE roles IS 'Список ролей пользователей';
COMMENT ON COLUMN roles.id IS 'Идентификатор роли';
COMMENT ON COLUMN roles.name IS 'Название роли';
COMMENT ON COLUMN roles.description IS 'Описание роли';



COMMENT ON TABLE driver_licenses IS 'Список водительских прав пользователей';
COMMENT ON COLUMN driver_licenses.id IS 'Идентификатор водительских прав';
COMMENT ON COLUMN driver_licenses.number IS 'Номер водительских прав';
COMMENT ON COLUMN driver_licenses.issue_date IS 'Дата выдачи';
COMMENT ON COLUMN driver_licenses.expire_date IS 'Срок действия';
COMMENT ON COLUMN driver_licenses.user_id IS 'Идентификатор владельца(пользователя)';



COMMENT ON TABLE categories IS 'Список категорий водительских прав';
COMMENT ON COLUMN categories.id IS 'Идентификатор категории';
COMMENT ON COLUMN categories.name IS 'Название категории';
COMMENT ON COLUMN categories.description IS 'Описание категории';



COMMENT ON TABLE xref_user_2_role IS 'Связи пользователей и их ролей';
COMMENT ON COLUMN xref_user_2_role.user_id IS 'Идентификатор пользователя';
COMMENT ON COLUMN xref_user_2_role.role_id IS 'Идентификатор роли';



COMMENT ON TABLE xref_driver_license_2_category IS 'Связь водительских прав и их категорий';
COMMENT ON COLUMN xref_driver_license_2_category.driver_license_id IS 'Идентификатор водительских прав';
COMMENT ON COLUMN xref_driver_license_2_category.category_id IS 'Идентификатор категории';

