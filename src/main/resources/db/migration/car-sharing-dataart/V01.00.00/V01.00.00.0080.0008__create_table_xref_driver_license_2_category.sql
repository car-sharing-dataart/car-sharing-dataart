CREATE TABLE xref_driver_license_2_category(
    driver_license_id uuid NOT NULL,
    category_id uuid NOT NULL,

    CONSTRAINT fk_xref_driver_license_2_category_2_driver_license
        FOREIGN KEY(driver_license_id)
            REFERENCES driver_licenses(id),
    CONSTRAINT fk_xref_driver_license_2_category_2_category
        FOREIGN KEY(category_id)
            REFERENCES categories(id)
);