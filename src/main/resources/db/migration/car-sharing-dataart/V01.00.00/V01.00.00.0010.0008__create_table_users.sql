CREATE TABLE users(
    id           uuid               NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT pk_user PRIMARY KEY (id),
    first_name   varchar(32)        NOT NULL,
    last_name    varchar(32)        NOT NULL,
    middle_name  varchar(32)        NOT NULL,
    login        varchar(32) UNIQUE NOT NULL,
    password     varchar(128)        NOT NULL,
    verified     boolean            DEFAULT FALSE      NOT NULL,
    birth_date   timestamp          NOT NULL,
    email        varchar(64) UNIQUE NOT NULL,
    mobile_phone varchar(16) UNIQUE NOT NULL,
    sex          varchar(8)         NOT NULL,
    create_ts    timestamp   DEFAULT (now() AT TIME ZONE 'UTC') NOT NULL,
    avatar_path  varchar(128),
    driver_license_id uuid   UNIQUE
);