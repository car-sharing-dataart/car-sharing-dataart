CREATE TABLE cars
(
    id          uuid               NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT pk_car PRIMARY KEY (id),
    brand       varchar(32)        NOT NULL,
    model       varchar(32)        NOT NULL,
    number      varchar(32) UNIQUE NOT NULL,
    vin         varchar(32) UNIQUE NOT NULL,
    year        timestamp          NOT NULL,
    colour      varchar(30)        NOT NULL,
    type        varchar(64)        NOT NULL,
    city        varchar(32)        NOT NULL,
    user_id     uuid               NOT NULL
);