CREATE TABLE driver_licenses(
    id          uuid               NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT pk_driver_license PRIMARY KEY (id),
    number varchar(16)    UNIQUE NOT NULL,
    issue_date timestamp  NOT NULL,
    expire_date timestamp NOT NULL,
    user_id    uuid       UNIQUE NOT NULL,

    CONSTRAINT fk_driver_license_2_user
        FOREIGN KEY (user_id)
            REFERENCES users(id)
);