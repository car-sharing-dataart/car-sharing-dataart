CREATE TABLE categories(
    id          uuid               NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT pk_category PRIMARY KEY (id),
    name        varchar(32) NOT NULL,
    description varchar(500)
);