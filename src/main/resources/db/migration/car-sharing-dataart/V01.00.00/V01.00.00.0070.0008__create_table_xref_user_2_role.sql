CREATE TABLE xref_user_2_role(
    user_id uuid NOT NULL,
    role_id uuid NOT NULL ,

    CONSTRAINT fk_xref_user_2_role_2_user
        FOREIGN KEY (user_id)
            REFERENCES users(id),

    CONSTRAINT fk_xref_user_2_role_2_role
        FOREIGN KEY (role_id)
            REFERENCES roles(id)
);


