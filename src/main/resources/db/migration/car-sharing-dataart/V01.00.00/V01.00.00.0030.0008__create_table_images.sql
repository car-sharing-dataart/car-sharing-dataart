CREATE TABLE images(
    id      uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT pk_image PRIMARY KEY (id),
    url varchar(128) NOT NULL,
    car_id  uuid NOT NULL
);
