CREATE TABLE roles(
    id          uuid               NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT pk_role PRIMARY KEY (id),
    name        varchar(64) UNIQUE NOT NULL,
    description varchar(500)
);