ALTER TABLE users
    RENAME sex TO sex_old;

ALTER TABLE users
    ADD COLUMN sex int NOT NULL CHECK (sex IN (0, 1, 2)) default 0;


UPDATE users
SET sex = (CASE
               WHEN sex_old = 'male'
                   THEN 1
               WHEN sex_old = 'female'
                   THEN 2
               ELSE 0
    END);

ALTER TABLE users DROP COLUMN sex_old;

