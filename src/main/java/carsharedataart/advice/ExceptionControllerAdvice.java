package carsharedataart.advice;

import carsharedataart.advice.exception.CarCreationException;
import carsharedataart.advice.exception.CarNotFoundException;
import carsharedataart.advice.exception.ImageCreationException;
import carsharedataart.advice.exception.ImageNotFoundException;
import carsharedataart.advice.exception.RoleNotFoundException;
import carsharedataart.advice.exception.UserCreationException;
import carsharedataart.advice.exception.UserNotFoundException;
import carsharedataart.controller.dto.AdviceDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<AdviceDTO> userNotExistsHandle(UserNotFoundException exception){
        return new ResponseEntity<>(new AdviceDTO(HttpStatus.BAD_REQUEST.value(),
                exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserCreationException.class)
    public ResponseEntity<AdviceDTO> noSuchFieldsHandle(UserCreationException exception){
        return new ResponseEntity<>(new AdviceDTO(HttpStatus.BAD_REQUEST.value(),
                exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CarCreationException.class)
    public ResponseEntity<AdviceDTO> carCreationHandle(CarCreationException exception){
        return new ResponseEntity<>(new AdviceDTO(HttpStatus.BAD_REQUEST.value(),
                exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CarNotFoundException.class)
    public ResponseEntity<AdviceDTO> carNotFoundHandle(CarNotFoundException exception){
        return new ResponseEntity<>(new AdviceDTO(HttpStatus.BAD_REQUEST.value(),
                exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RoleNotFoundException.class)
    public ResponseEntity<AdviceDTO> roleNotFoundHandle(RoleNotFoundException exception){
        return new ResponseEntity<>(new AdviceDTO(HttpStatus.BAD_REQUEST.value(),
                exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ImageCreationException.class)
    public ResponseEntity<AdviceDTO> imageCreationHandle(ImageCreationException exception){
        return new ResponseEntity<>(new AdviceDTO(HttpStatus.BAD_REQUEST.value(),
                exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ImageNotFoundException.class)
    public ResponseEntity<AdviceDTO> imageNotFoundHandle(ImageNotFoundException exception){
        return new ResponseEntity<>(new AdviceDTO(HttpStatus.BAD_REQUEST.value(),
                exception.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
