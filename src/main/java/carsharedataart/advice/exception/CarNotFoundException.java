package carsharedataart.advice.exception;

public class CarNotFoundException extends RuntimeException {

    private final String message;

    public CarNotFoundException(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
