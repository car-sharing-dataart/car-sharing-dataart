package carsharedataart.advice.exception;

public class CarCreationException extends RuntimeException {

    private final String message;

    public CarCreationException(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

