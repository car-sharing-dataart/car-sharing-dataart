package carsharedataart.advice.exception;

public class ImageNotFoundException extends RuntimeException {

    private final String message;

    public ImageNotFoundException(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
