package carsharedataart.advice.exception;

public class ImageCreationException extends RuntimeException {

    private final String message;

    public ImageCreationException(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
