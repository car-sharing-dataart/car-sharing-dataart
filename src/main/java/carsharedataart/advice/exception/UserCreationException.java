package carsharedataart.advice.exception;

public class UserCreationException extends RuntimeException {

    private final String message;

    public UserCreationException(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
