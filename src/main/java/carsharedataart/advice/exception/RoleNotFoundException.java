package carsharedataart.advice.exception;

public class RoleNotFoundException extends RuntimeException {

    private final String message;

    public RoleNotFoundException(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
