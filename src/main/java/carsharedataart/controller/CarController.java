package carsharedataart.controller;

import carsharedataart.controller.converter.CarConverter;
import carsharedataart.controller.dto.CarDTO;
import carsharedataart.service.CarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import static carsharedataart.config.WebSecurityConfig.*;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/cars")
@Api(value = "Car controller", description = "Controller for managing cars")
public class CarController {

    private final CarService carService;
    private final CarConverter carConverter;

    @Autowired
    public CarController(
            final CarService carService,
            final CarConverter carConverter
    ) {
        this.carService = carService;
        this.carConverter = carConverter;
    }

    @ApiOperation(value = "Method return car by UUID")
    @GetMapping("/{id}")
    public CarDTO getById(@PathVariable(name = "id") final UUID id) {
        return carConverter.toDTO(carService.getById(id));
    }

    @ApiOperation(value = "Method return list of cars with data")
    @GetMapping
    public List<CarDTO> getAll() {
        return carConverter.toDtoList(carService.getAll());
    }

    @ApiOperation(value = "Method save car if user verified")
    @PostMapping("/create")
    @PreAuthorize(AUTH_USER_RULES)
    public CarDTO create(@RequestBody @Valid final CarDTO car) {
        return carConverter.toDTO(carService.create(carConverter.toEntity(car)));
    }

    @ApiOperation(value = "Method update car data")
    @PutMapping("/update")
    @PreAuthorize(AUTH_USER_RULES)
    public CarDTO update(@RequestBody @Valid final CarDTO car) {
        return carConverter.toDTO(carService.update(carConverter.toEntity(car)));
    }

    @ApiOperation(value = "Method delete car by UUID")
    @DeleteMapping("/{id}")
    @PreAuthorize(AUTH_USER_RULES)
    public UUID delete(@PathVariable(name = "id") final UUID id) {
        return carService.delete(id);
    }
}