package carsharedataart.controller;

import carsharedataart.controller.converter.ImageConverter;
import carsharedataart.controller.dto.ImageDTO;
import carsharedataart.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/images")
@Api(value = "Image controller", description = "Controller for managing images")
public class ImageController {

    private final ImageService imageService;
    private final ImageConverter imageConverter;

    @Autowired
    public ImageController(
            final ImageService imageService,
            final ImageConverter imageConverter
    ) {
        this.imageService = imageService;
        this.imageConverter = imageConverter;
    }

    @ApiOperation(value = "Method return image by UUID")
    @GetMapping("/{id}")
    public ImageDTO getById(@PathVariable(name = "id") final UUID id) {
        return imageConverter.toDTO(imageService.getById(id));
    }

    @ApiOperation(value = "Method return list of images with data")
    @GetMapping
    public List<ImageDTO> getAll() {
        return imageConverter.toDtoList(imageService.getAll());
    }

    @ApiOperation(value = "Method save image")
    @PostMapping("/create")
    public ImageDTO create(@RequestBody final ImageDTO imageDTO) {
        return imageConverter.toDTO(imageService.create(imageConverter.toEntity(imageDTO)));
    }

    @ApiOperation(value = "Method update image data")
    @PutMapping("/update")
    public ImageDTO update(@RequestBody final ImageDTO imageDTO) {
        return imageConverter.toDTO(imageService.update(imageConverter.toEntity(imageDTO)));
    }

    @ApiOperation(value = "Method delete image by UUID")
    @DeleteMapping("/{id}")
    public UUID delete(@PathVariable(name = "id") final UUID id) {
        return imageService.delete(id);
    }
}