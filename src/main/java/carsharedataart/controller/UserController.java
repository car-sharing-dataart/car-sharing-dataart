package carsharedataart.controller;

import carsharedataart.controller.converter.UserConverter;
import carsharedataart.controller.dto.UserDTO;
import carsharedataart.controller.dto.UserLoginDTO;
import carsharedataart.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import static carsharedataart.config.WebSecurityConfig.*;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/users")
@Api(value = "User controller", description = "Controller for managing users")
public class UserController {

    private final UserService userService;
    private final UserConverter userConverter;

    @Autowired
    public UserController(
            final UserService userService,
            final UserConverter userConverter
    ) {
        this.userService = userService;
        this.userConverter = userConverter;
    }

    @ApiOperation(value = "Method return user by UUID")
    @GetMapping("/{id}")
    @PreAuthorize(AUTH_ADMIN_ONLY_RULES)
    public UserDTO getById(@PathVariable(name = "id") final UUID id) {
        return userConverter.toDTO(userService.getById(id));
    }

    @ApiOperation(value = "Method return list of users with data")
    @GetMapping
    @PreAuthorize(AUTH_ADMIN_ONLY_RULES)
    public List<UserDTO> getAll() {
        return userConverter.toDtoList(userService.getAll());
    }

    @ApiOperation(value = "Method update user data")
    @PutMapping("/update")
    @PreAuthorize(AUTH_USER_RULES)
    public UserDTO update(@RequestBody @Valid final UserDTO userDTO) {
        return userConverter.toDTO(userService.update(userConverter.toEntity(userDTO)));
    }

    @ApiOperation(value = "Method delete user by UUID")
    @DeleteMapping("/{id}")
    @PreAuthorize(AUTH_USER_RULES)
    public UUID delete(@PathVariable(name = "id") final UUID id) {
       return userService.delete(id);
    }

    @ApiOperation(value = "Method save new user")
    @PostMapping("/create")
    public UserDTO create(@RequestBody @Valid final UserDTO userDTO) {
        return userConverter.toDTO(userService.create(userConverter.toEntity(userDTO)));
    }

    @ApiOperation(value = "Method authorize user")
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid final UserLoginDTO userLoginDTO) {
        return ResponseEntity.ok(userService.login(userLoginDTO.getLogin(), userLoginDTO.getPassword()));
    }
}
