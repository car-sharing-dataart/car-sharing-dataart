package carsharedataart.controller.converter;

import carsharedataart.controller.dto.RoleDTO;
import carsharedataart.database.entity.Role;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RoleConverter {

    private final ModelMapper modelMapper = new ModelMapper();

    public RoleDTO toDTO(final Role role) {
        return modelMapper.map(role, RoleDTO.class);
    }

    public Role toEntity(final RoleDTO roleDTO) {
        return modelMapper.map(roleDTO, Role.class);
    }

    public List<RoleDTO> toDtoList(final List<Role> roles){
        return roles.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
