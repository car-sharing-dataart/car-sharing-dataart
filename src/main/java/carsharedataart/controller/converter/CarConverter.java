package carsharedataart.controller.converter;

import carsharedataart.controller.dto.CarDTO;
import carsharedataart.database.entity.Car;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CarConverter {

    private final ModelMapper modelMapper = new ModelMapper();

    public CarDTO toDTO(final Car car) {
        return modelMapper.map(car, CarDTO.class);
    }

    public Car toEntity(final CarDTO carDTO) {
        return modelMapper.map(carDTO, Car.class);
    }

    public List<CarDTO> toDtoList(final List<Car> cars) {
        return cars.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
