package carsharedataart.controller.converter;

import carsharedataart.controller.dto.ImageDTO;
import carsharedataart.database.entity.Image;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ImageConverter {

    private final ModelMapper modelMapper = new ModelMapper();

    public ImageDTO toDTO(final Image image) {
        return modelMapper.map(image, ImageDTO.class);
    }

    public Image toEntity(final ImageDTO imageDTO) {
        return modelMapper.map(imageDTO, Image.class);
    }

    public List<ImageDTO> toDtoList(final List<Image> images){
        return images.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
