package carsharedataart.controller.converter;

import carsharedataart.controller.dto.DriverLicenseDTO;
import carsharedataart.database.entity.DriverLicense;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DriverLicenseConverter {

    private final ModelMapper modelMapper = new ModelMapper();

    public DriverLicenseDTO toDTO(final DriverLicense driverLicense) {
        return modelMapper.map(driverLicense, DriverLicenseDTO.class);
    }

    public DriverLicense toEntity(final DriverLicenseDTO driverLicenseDTO) {
        return modelMapper.map(driverLicenseDTO, DriverLicense.class);
    }

    public List<DriverLicenseDTO> toDtoList(final List<DriverLicense> driverLicenses){
        return driverLicenses.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
