package carsharedataart.controller.converter;

import carsharedataart.controller.dto.UserDTO;
import carsharedataart.database.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter {

    private final ModelMapper modelMapper = new ModelMapper();

    public UserDTO toDTO(final User user) {
        return modelMapper.map(user, UserDTO.class);
    }

    public User toEntity(final UserDTO userDTO) {
        return modelMapper.map(userDTO, User.class);
    }

    public List<UserDTO> toDtoList(final List<User> users){
        return users.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
