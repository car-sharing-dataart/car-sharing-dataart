package carsharedataart.controller.converter;

import carsharedataart.controller.dto.CategoryDTO;
import carsharedataart.database.entity.Category;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryConverter {

    private final ModelMapper modelMapper = new ModelMapper();

    public CategoryDTO toDTO(final Category category) {
        return modelMapper.map(category, CategoryDTO.class);
    }

    public Category toEntity(final CategoryDTO categoryDTO) {
        return modelMapper.map(categoryDTO, Category.class);
    }

    public List<CategoryDTO> toDtoList(final List<Category> categories){
        return categories.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
