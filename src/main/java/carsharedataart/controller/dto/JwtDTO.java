package carsharedataart.controller.dto;

import carsharedataart.enums.Sex;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@RequiredArgsConstructor
public class JwtDTO {

    @NonNull
    private String token;

    private String type = "Bearer";

    @NonNull
    private UUID id;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private String middleName;

    @NonNull
    private String email;

    @NonNull
    private Boolean verified;

    @NonNull
    private LocalDateTime birthDate;

    @NonNull
    private LocalDateTime creatTs;

    @NonNull
    private Sex sex;

    @NonNull
    private String avatarPath;

    @NonNull
    private List<String> roles;
}
