package carsharedataart.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User login")
public class UserLoginDTO {

    @NotNull(message = "Login name can`t be null or empty")
    @ApiModelProperty(value = "User login", example = "alex2021", required = true)
    private String login;

    @NotNull(message = "Password name can`t be null or empty")
    @ApiModelProperty(value = "User password", example = "password", required = true)
    private String password;
}
