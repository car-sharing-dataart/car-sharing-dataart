package carsharedataart.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Role")
public class RoleDTO {

    private UUID id;

    @NotEmpty(message = "Role name con not be empty.")
    @Size(max = 64, message = "Max role name is 64 characters.")
    @ApiModelProperty(value = "Role name", example = "USER", required = true)
    private String name;

    @Size(max = 500, message = "Max role description is 500 characters.")
    @ApiModelProperty(value = "Role description", example = "Role for regular user")
    private String description;
}
