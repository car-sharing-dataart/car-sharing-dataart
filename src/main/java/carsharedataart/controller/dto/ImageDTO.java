package carsharedataart.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Image")
public class ImageDTO {

    private UUID id;

    @NotEmpty(message = "URL can not be empty.")
    @Size(max = 128, message = "Max url length is 128 characters.")
    @ApiModelProperty(value = "Image url", example = "59168236-5259-4c17-b826-d0620f0d99a7", required = true)
    private String url;

    @JsonIgnore
    private CarDTO car;
}
