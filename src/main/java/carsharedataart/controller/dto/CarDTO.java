package carsharedataart.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Car")
public class CarDTO {

    private UUID id;

    @NotEmpty(message = "Car brand can not be empty.")
    @Size(min = 2, max = 32, message = "Field length from 2 to 32 characters")
    @ApiModelProperty(value = "Car brand", example = "Audi", required = true)
    private String brand;

    @NotEmpty(message = "Car model can not be empty.")
    @Size(min = 2, max = 32, message = "Field length from 2 to 32 characters")
    @ApiModelProperty(value = "Car model", example = "A6", required = true)
    private String model;

    @NotEmpty
    @Pattern(regexp = "^[ABCEHIKMOPTX]{2}\\d{4}(?<!0{4})[ABCEHIKMOPTX]{2}$", message = "Car number does not match.")
    @ApiModelProperty(value = "Car number", example = "AA0101BB", required = true)
    private String number;

    @NotEmpty
    @Pattern(regexp = "^[A-HJ-NPR-Za-hj-npr-z\\d]{8}[\\dX][A-HJ-NPR-Za-hj-npr-z\\d]{2}\\d{6}$", message = "VIN does not match.")
    @ApiModelProperty(value = "VIN", example = "WDB1240191J016310", required = true)
    private String vin;

    @NotNull
    @ApiModelProperty(value = "Year", example = "2000-10-10T00:00:00", required = true)
    private LocalDateTime year;

    @NotEmpty
    @Size(min = 3, max = 30, message = "Field length from 2 to 30 characters")
    @ApiModelProperty(value = "Car colour", example = "Red", required = true)
    private String colour;

    @NotEmpty
    @Size(min = 3, max = 64, message = "Field length from 2 to 64 characters")
    @ApiModelProperty(value = "Car type", example = "Coupe", required = true)
    private String type;

    @NotEmpty
    @Size(min = 3, max = 32, message = "Field length from 2 to 32 characters")
    @ApiModelProperty(value = "Car city location", example = "Kiev", required = true)
    private String city;

    @Valid
    @ApiModelProperty(value = "Car images")
    private Set<ImageDTO> images;

    @NotNull
    @ApiModelProperty(value = "Owner UUID", example = "bc9aa51a-af77-48c5-8cb5-38e1d3f52b3b", required = true)
    private UUID userId;
}
