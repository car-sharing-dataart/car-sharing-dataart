package carsharedataart.controller.dto;

import carsharedataart.enums.Sex;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User")
public class UserDTO {

    private UUID id;

    @NotEmpty(message = "First name can not be null or empty.")
    @Size(min = 2, max = 32, message = "Field length from 2 to 32 characters.")
    @Pattern(regexp = "^[A-Z][a-z]+$", message = "First name must start with a capital letter.")
    @ApiModelProperty(value = "User first name", example = "Alex", required = true)
    private String firstName;

    @NotEmpty(message = "Last name can not be null or empty.")
    @Size(min = 2, max = 32, message = "Field length from 2 to 32 characters.")
    @Pattern(regexp = "^[A-Z][a-z]+$", message = "Last name must start with a capital letter.")
    @ApiModelProperty(value = "User last name", example = "Smith", required = true)
    private String lastName;

    @NotEmpty(message = "Middle name can not be null or empty.")
    @Size(min = 2, max = 32, message = "Field length from 2 to 32 characters.")
    @Pattern(regexp = "^[A-Z][a-z]+$", message = "Middle name must start with a capital letter.")
    @ApiModelProperty(value = "User middle name", example = "John", required = true)
    private String middleName;

    @NotEmpty(message = "Login can not be null or empty.")
    @Size(min = 8, max = 32, message = "Field length from 8 to 32 characters.")
    @ApiModelProperty(value = "User login", example = "alex2021", required = true)
    private String login;

    @NotEmpty(message = "Password can not be null or empty.")
    @Size(min = 8, max = 128, message = "Field length from 8 to 128 characters.")
    @ApiModelProperty(value = "User password", example = "password", required = true)
    private String password;

    @NotNull(message = "Verified can not be null or empty.")
    @ApiModelProperty(value = "Is user verified")
    private Boolean verified;

    @NotNull(message = "Birthdate can not be null or empty.")
    @ApiModelProperty(value = "User birth date", example = "2015-10-10T00:00:00", required = true)
    private LocalDateTime birthDate;

    @Email(message = "Email does not match.")
    @Size(max = 64)
    @ApiModelProperty(value = "User email", example = "alex@gmail.com", required = true)
    private String email;

    @NotEmpty
    @Pattern(regexp = "^+?3?8?(0\\d{9})$", message = "Mobile phone does not match.")
    @ApiModelProperty(value = "User mobile phone", example = "380672227777", required = true)
    private String mobilePhone;

    @NotNull(message = "Sex can not be empty.")
    @ApiModelProperty(value = "User sex", example = "MALE", required = true)
    private Sex sex;

    @NotNull(message = "Creation time can not be empty.")
    @ApiModelProperty(value = "Creation time")
    private LocalDateTime creatTs;

    @NotEmpty
    @Size(max = 128)
    @ApiModelProperty(value = "Avatar url", example = "59168236-5259-4c17-b826-d0620f0d99a7", required = true)
    private String avatarPath;

    @Valid
    @ApiModelProperty(value = "User driver license")
    private DriverLicenseDTO driverLicense;

    @Valid
    @ApiModelProperty(value = "User cars")
    private Set<CarDTO> cars;

    @Valid
    @ApiModelProperty(value = "User roles")
    private Set<RoleDTO> roles;
}
