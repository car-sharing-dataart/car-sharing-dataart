package carsharedataart.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Driver licence")
public class DriverLicenseDTO {

    private UUID id;

    @ApiModelProperty(value = "Driver licence number", example = "BXE 513613", required = true)
    private String number;

    @ApiModelProperty(value = "Driver licence issue date", example = "2015-10-10T00:00:00", required = true)
    private LocalDateTime issueDate;

    @ApiModelProperty(value = "Driver licence expire date", example = "2025-10-10T00:00:00", required = true)
    private LocalDateTime expireDate;

    @ApiModelProperty(value = "Driver licence categories", required = true)
    private Set<CategoryDTO> categories;

    @ApiModelProperty(value = "Driver licence owner", required = true)
    private UserDTO user;
}
