package carsharedataart.controller.dto;

import carsharedataart.database.entity.User;
import carsharedataart.enums.Sex;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Implementation of user details with no sensitive user information
 */
@Getter
@Setter
@RequiredArgsConstructor
public class UserDetailsDTO implements UserDetails {

    @NonNull
    private UUID id;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private String middleName;

    @NonNull
    private String email;

    @NonNull
    private Boolean verified;

    @NonNull
    private LocalDateTime birthDate;

    @NonNull
    private LocalDateTime creatTs;

    @NonNull
    private Sex sex;

    @NonNull
    @JsonIgnore
    private String login;

    @NonNull
    @JsonIgnore
    private String password;

    @NonNull
    private String avatarPath;

    @NonNull
    private Collection<? extends GrantedAuthority> authorities;


    public static UserDetailsDTO build(User user) {
        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());

        return new UserDetailsDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getMiddleName(),
                user.getEmail(),
                user.getVerified(),
                user.getBirthDate(),
                user.getCreatTs(),
                user.getSex(),
                user.getLogin(),
                user.getPassword(),
                user.getAvatarPath(),
                authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
