package carsharedataart.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Driver licence category")
public class CategoryDTO {

    private UUID id;

    @ApiModelProperty(value = "Category name", example = "B", required = true)
    private String name;

    @ApiModelProperty(value = "Category description", example = "Vehicles up to 3,500kg")
    private String description;
}
