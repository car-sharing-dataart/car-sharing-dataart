package carsharedataart.config.jwt;

import carsharedataart.service.CustomUserDetailsService;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter base class that aims to guarantee a single execution per request dispatch
 */
public class AuthTokenFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);
    private static final int TOKEN_HEADER_LENGTH = 7;

    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    /**
     * Stores a request attribute for "already filtered",
     * proceeding without filtering again if the attribute is already there
     * but guaranteed to be just invoked once per request within a single request thread.
     *
     * @param request                                       HttpServletRequest
     * @param response                                      HttpServletResponse
     * @param filterChain                                   FilterChain
     * @throws ServletException                             on error
     * @throws IOException                                  on error
     */
    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {
        try {
            String jwt = parseJwt(request);
            if (jwt != null && jwtConfig.validateJwtToken(jwt)) {
                String username = jwtConfig.getUserNameFromJwtToken(jwt);
                UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception e) {
            logger.debug("Filter internal error: " + e.getMessage());
        }

        filterChain.doFilter(request, response);
    }

    /**
     * Method accepts HttpServletRequest and parse jwt
     *
     * @param request                                   HttpServletRequest
     * @return                                          String parseJwt on successful parse
     */
    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(TOKEN_HEADER_LENGTH);
        }
        return null;
    }
}
