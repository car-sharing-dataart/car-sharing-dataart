package carsharedataart.config.jwt;

import carsharedataart.controller.dto.UserDetailsDTO;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import java.util.Date;

/**
 * JWT token configuration
 */
@Component
public class JwtConfig {

    @Value("${carshare.app.jwtSecret}")
    private String jwtSecret;

    @Value("${carshare.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    /**
     * Method accepts authentication request and generate jwt by user login and password
     *
     * @param authentication                        Authentication
     * @return                                      String generateJwtToken
     */
    public String generateJwtToken(Authentication authentication) {

        UserDetailsDTO userPrincipal = (UserDetailsDTO) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .claim("authorities", userPrincipal.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * Method parse jwt token and return username
     *
     * @param token                                 JWT token
     * @return                                      username from JWT token
     */
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * Method validate JWT token from authentication
     *
     * @param authToken                             JWT token from authentication
     * @return                                      result after validation
     */
    public boolean validateJwtToken(final String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException | MalformedJwtException |
                ExpiredJwtException | UnsupportedJwtException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return false;
    }
}
