package carsharedataart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("carsharedataart.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "CAR-SHARING-DATAART REST API",
                "This project is created for improving skills and demonstration\n" +
                        "of developer proficiency.\n" +
                        "\n" +
                        "The main purposes of creation are:\n" +
                        "\n" +
                        "- give car-sharing functionality for users;\n" +
                        "- to provide easy to understand application for deals which include temporary usage of another user`s car for money compensation;\n" +
                        "- fast and high-quality solution of disputable situations between users;\n" +
                        "- ensuring the confidentiality of communication between users.",
                "01.02.00",
                "Terms of service",
                new Contact("Denys Suk", "localhost:3000/home", "denis133713372@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
