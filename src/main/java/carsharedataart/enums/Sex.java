package carsharedataart.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum Sex {

    NOT_ASSIGNED(0),
    MALE(1),
    FEMALE(2);

    private int id;
}
