package carsharedataart.database.entity;

import carsharedataart.enums.Sex;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(
            name = "uuid4",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private UUID id;

    @Column(name = "first_name", nullable = false, length = 32)
    private String firstName;

    @Column(name = "last_name", nullable = false, length = 32)
    private String lastName;

    @Column(name = "middle_name", nullable = false, length = 32)
    private String middleName;

    @Column(name = "login", nullable = false, unique = true, length = 32)
    private String login;

    @Column(name = "password", nullable = false, length = 128)
    private String password;

    @Column(name = "verified", nullable = false)
    private Boolean verified;

    @Column(name = "birth_date", nullable = false)
    private LocalDateTime birthDate;

    @Column(name = "email", unique = true, length = 64, nullable = false)
    private String email;

    @Column(name = "mobile_phone", unique = true, nullable = false, length = 16)
    private String mobilePhone;

    @Column(name = "sex", nullable = false, length = 8)
    private Sex sex;

    @Column(name = "create_ts", nullable = false)
    private LocalDateTime creatTs;

    @Column(name = "avatar_path", length = 128)
    private String avatarPath;

    @OneToOne
    private DriverLicense driverLicense;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "user")
    private Set<Car> cars;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "xref_user_2_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<Role> roles;
}

