package carsharedataart.database.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "driver_licenses")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DriverLicense {

    @Id
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private UUID id;

    @Column(name = "number", unique = true, nullable = false, length = 16)
    private String number;

    @Column(name = "issue_date", nullable = false)
    private LocalDateTime issueDate;

    @Column(name = "expire_date", nullable = false)
    private LocalDateTime expireDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "xref_driver_license_2_category",
            joinColumns = {@JoinColumn(name = "driver_license_id")},
            inverseJoinColumns = {@JoinColumn(name = "category_id")})
    private Set<Category> categories;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}
