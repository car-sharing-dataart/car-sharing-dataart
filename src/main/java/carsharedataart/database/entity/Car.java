package carsharedataart.database.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "cars")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Car {

    @Id
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private UUID id;

    @Column(name = "brand", nullable = false, length = 32)
    private String brand;

    @Column(name = "model", nullable = false, length = 32)
    private String model;

    @Column(name = "number",unique = true, nullable = false, length = 32)
    private String number;

    @Column(name = "vin",unique = true, nullable = false, length = 32)
    private String vin;

    @Column(name = "year", nullable = false)
    private LocalDateTime year;

    @Column(name = "colour", nullable = false, length = 30)
    private String colour;

    @Column(name = "type", nullable = false, length = 64)
    private String type;

    @Column(name = "city", nullable = false, length = 32)
    private String city;

    @OneToMany(mappedBy = "car", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Image> images;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
