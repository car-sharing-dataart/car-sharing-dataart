package carsharedataart.database.repository;

import carsharedataart.database.entity.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface CarRepository extends CrudRepository<Car, UUID> {

}