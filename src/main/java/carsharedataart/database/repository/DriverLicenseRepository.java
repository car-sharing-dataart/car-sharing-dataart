package carsharedataart.database.repository;

import carsharedataart.database.entity.DriverLicense;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface DriverLicenseRepository extends CrudRepository<DriverLicense, UUID> {

}
